package de.theyphania.theyphaniachat.utils;

import de.theyphania.theyphaniachat.TheyphaniaChat;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.cacheddata.CachedMetaData;
import net.luckperms.api.cacheddata.CachedPermissionData;
import net.luckperms.api.platform.PlayerAdapter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static net.md_5.bungee.api.ChatColor.DARK_GRAY;
import static net.md_5.bungee.api.ChatColor.GRAY;
import static net.md_5.bungee.api.ChatColor.WHITE;

public class TheyphaniaPlayer
{
	private final Player player;
	private final boolean isFemale;
	private final ChatColor standColor;
	private final ChatColor factionColor;
	private final ChatColor rankColor;
	private final String stand;
	private final String title;
	private final String faction;
	private final String job;
	private final String rank;
	private final String prefix;
	private final String sortOrder;
	private final List<BaseComponent> roles;
	private final String[] names;
	private final CachedMetaData playerMetaData;
	private final CachedPermissionData playerPermissionData;
	
	public TheyphaniaPlayer(Player player)
	{
		this.player = player;
		
		PlayerAdapter<Player> playerAdapter = LuckPermsProvider.get().getPlayerAdapter(Player.class);
		this.playerMetaData = playerAdapter.getMetaData(player);
		this.playerPermissionData = playerAdapter.getPermissionData(player);
		
		this.isFemale = playerMetaData.getMetaValue("isfemale") != null;
		this.standColor = ChatColor.of(replaceColorNull(playerMetaData.getMetaValue("standcolor")));
		this.factionColor = ChatColor.of(replaceColorNull(playerMetaData.getMetaValue("factioncolor")));
		this.rankColor = ChatColor.of(replaceColorNull(playerMetaData.getMetaValue("rankcolor")));
		this.stand = getPlayerMeta("stand");
		this.title = getPlayerMeta(isFemale ? "female" : "male");
		this.faction = getPlayerMeta("faction");
		this.job = getPlayerMeta("job" + (isFemale ? "female" : "male"));
		this.rank = getPlayerMeta("rank");
		this.prefix = ChatColor.translateAlternateColorCodes('&', replaceNull(playerMetaData.getPrefix()));
		this.roles = initializeRoles();
		this.names = initializeNames();
		this.sortOrder = calculateSortOrder();
	}
	
	private String calculateSortOrder()
	{
		String temp = getPlayerMeta("sortorder");
		if (temp.isEmpty()) return "99999";
		if (temp.length() > 5) return "99999";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < (5 - temp.length()); i++)
		{
			sb.append("0");
		}
		sb.append(temp);
		return sb.toString();
	}
	
	public String getSortOrder()
	{
		return sortOrder;
	}
	
	public org.bukkit.ChatColor getBukkitStandColor()
	{
		return org.bukkit.ChatColor.valueOf(replaceColorNull(playerMetaData.getMetaValue("standcolor")).toUpperCase());
	}
	
	private String[] initializeNames()
	{
		String[] names = new String[6];
		names[0] = getPlayerMeta("vorname1");
		names[1] = getPlayerMeta("vorname2");
		names[2] = getPlayerMeta("vorname3");
		names[3] = getPlayerMeta("vorname4");
		names[4] = getPlayerMeta("adelszusatz");
		names[5] = getPlayerMeta("nachname");
		return names;
	}
	
	public String getRufname()
	{
		return names[Integer.parseInt(getPlayerMeta("rufname").isEmpty() ? "1" : getPlayerMeta("rufname")) - 1];
	}
	
	public void setPlayerMetaData(String key, String value)
	{
		CommandSender console = TheyphaniaChat.get().getServer().getConsoleSender();
		Bukkit.dispatchCommand(console, "lp user " + this.player.getDisplayName() + " meta set " + key + " " + value);
	}
	
	public String getAdelszusatz()
	{
		return names[4];
	}
	
	public String getNachname()
	{
		return names[5];
	}
	
	public String[] getNames()
	{
		return names;
	}
	
	public String[] getVornamen()
	{
		String[] vornamen = new String[4];
		vornamen[0] = names[0];
		vornamen[1] = names[1];
		vornamen[2] = names[2];
		vornamen[3] = names[3];
		return vornamen;
	}
	
	public String getFullName()
	{
		StringBuilder sb = new StringBuilder();
		for (String name : names)
		{
			sb.append(name.isEmpty() ? "" : name + " ");
		}
		return sb.toString();
	}
	
	private List<BaseComponent> initializeRoles()
	{
		List<String> ministries = new LinkedList<>();
		ministries.add("mla");
		ministries.add("moi");
		ministries.add("mqi");
		ministries.add("mrm");
		ministries.add("msg");
		ministries.add("mti");
		ministries.add("mwa");
		
		List<BaseComponent> tempList = new LinkedList<>();
		
		for (String ministry : ministries)
		{
			String role = getPlayerMeta("role" + ministry);
			if (role.isEmpty()) continue;
			String roleColor = getPlayerMeta("rolecolor" + ministry);
			TextComponent tempComponent = new TextComponent();
			tempComponent.setText(role);
			tempComponent.setColor(ChatColor.of(roleColor));
			tempList.add(tempComponent);
		}
		
		return tempList;
	}
	
	
	public List<BaseComponent> getRoles()
	{
		return roles;
	}
	
	public Player getPlayer()
	{
		return player;
	}
	
	public boolean isFemale()
	{
		return isFemale;
	}
	
	public boolean hasPermission(String permission)
	{
		return playerPermissionData.checkPermission(permission).asBoolean();
	}
	
	public String getFaction()
	{
		return faction;
	}
	
	public String getJob()
	{
		return job;
	}
	
	public String getRank()
	{
		return rank;
	}
	
	public String getStand()
	{
		return stand;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public String getPrefix()
	{
		return prefix;
	}
	
	public ChatColor getFactionColor()
	{
		return factionColor;
	}
	
	public ChatColor getRankColor()
	{
		return rankColor;
	}
	
	public ChatColor getStandColor()
	{
		return standColor;
	}
	
	public BaseComponent getNameWithHoverText()
	{
		ChatColor customYellow = ChatColor.of("#ecbb12");
		ChatColor customBlue = ChatColor.of("#008cff");
		ChatColor customGreen = ChatColor.of("#3fcf00");
		String newLine = "\n";
		TextComponent nameWithHoverTextComponent = new TextComponent(player.getDisplayName());
		nameWithHoverTextComponent.setColor(this.standColor);
		
		ComponentBuilder rolesBuilder = new ComponentBuilder();
		if (!getRoles().isEmpty())
		{
			for (BaseComponent c : getRoles())
			{
				rolesBuilder.append(c).append(", ").color(WHITE);
			}
			rolesBuilder.removeComponent(rolesBuilder.getCursor());
		}
		else
		{
			rolesBuilder.append("N/A").color(WHITE);
		}
		BaseComponent[] rolesComponent = rolesBuilder.create();
		
		BaseComponent[] hoverComponents = new ComponentBuilder("// ===== [").color(DARK_GRAY)
											  .append("usr").color(customYellow)
											  .append("::").color(WHITE)
											  .append("info").color(customBlue)
											  .append("::").color(WHITE)
											  .append("roleplay").color(customGreen)
											  .append("] ===== //").color(DARK_GRAY).append(newLine)
											  .append("Name: ").color(GRAY).append(replaceEmpty(getFullName())).color(WHITE).append(newLine)
											  .append("Stand: ").color(GRAY).append(replaceEmpty(getStand())).color(getStandColor()).append(newLine)
											  .append("Titel: ").color(GRAY).append(replaceEmpty(getTitle())).color(getStandColor()).append(newLine)
											  .append("Fraktion: ").color(GRAY).append(replaceEmpty(getFaction())).color(getFactionColor()).append(newLine)
											  .append("Beruf: ").color(GRAY).append(replaceEmpty(getJob())).color(getFactionColor()).append(newLine)
											  .append("// ===== [").color(DARK_GRAY)
											  .append("usr").color(customYellow)
											  .append("::").color(WHITE)
											  .append("info").color(customBlue)
											  .append("::").color(WHITE)
											  .append("theypha").color(customGreen)
											  .append("] ===== //").color(DARK_GRAY).append(newLine)
											  .append("Rang: ").color(GRAY).append(getRank()).color(getRankColor()).append(newLine)
											  .append("Rollen: ").color(GRAY).append(rolesComponent)
											  .create();
		
		HoverEvent he = new HoverEvent(Action.SHOW_TEXT, new Text(hoverComponents));
		nameWithHoverTextComponent.setHoverEvent(he);
		return nameWithHoverTextComponent;
	}
	
	private String getPlayerMeta(String meta)
	{
		return replaceNull(playerMetaData.getMetaValue(meta));
	}
	
	private String replaceNull(String toReplace)
	{
		return toReplace == null ? "" : toReplace;
	}
	
	private String replaceEmpty(String toReplace)
	{
		return toReplace.isEmpty() ? "N/A" : toReplace;
	}
	
	private String replaceColorNull(String toReplace)
	{
		return toReplace == null ? "white" : toReplace;
	}
}
