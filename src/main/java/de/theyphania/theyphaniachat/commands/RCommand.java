package de.theyphania.theyphaniachat.commands;

import de.theyphania.theyphaniachat.TheyphaniaChat;
import dev.jorel.commandapi.annotations.Command;
import dev.jorel.commandapi.annotations.Default;
import dev.jorel.commandapi.annotations.Permission;
import dev.jorel.commandapi.annotations.arguments.AGreedyStringArgument;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

@Command("r")
@Permission("theyphania.command.r")
public class RCommand
{
	private static final TheyphaniaChat plugin = TheyphaniaChat.get();
	
	@Default
	public static void r(CommandSender sender)
	{
		plugin.sendCustomMessage(sender, "Antworte innerhalb deiner letzten Konversation.");
	}
	
	@Default
	public static void r(CommandSender sender, @AGreedyStringArgument String message)
	{
		String targetName = plugin.getLastPmPlayer(sender.getName());
		if (targetName == null)
		{
			plugin.sendCustomMessage(sender, "Dafür musst du bereits eine offene Konversation haben.");
			return;
		}
		CommandSender target = targetName.equalsIgnoreCase("console") ? Bukkit.getConsoleSender() : Bukkit.getPlayer(targetName);
		if (target == null)
		{
			plugin.sendCustomMessage(sender, "Anscheinend ist der Cache nicht ganz UP-TO-DATE.");
			return;
		}
		plugin.setPlayersInPm(sender.getName(), targetName);
		sender.sendMessage(ChatColor.GREEN + "(Zu " + target.getName() + ") " + ChatColor.GRAY + message);
		target.sendMessage(ChatColor.GREEN + "(Von " + sender.getName() + ") " + ChatColor.GRAY + message);
	}
}
