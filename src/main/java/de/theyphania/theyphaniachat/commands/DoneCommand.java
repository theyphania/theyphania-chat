package de.theyphania.theyphaniachat.commands;

import de.theyphania.theyphaniachat.TheyphaniaChat;
import de.theyphania.theyphaniachat.utils.TheyphaniaChatPerms;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.Argument;
import dev.jorel.commandapi.arguments.StringArgument;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

public class DoneCommand
{
	private final TheyphaniaChat plugin = TheyphaniaChat.get();
	private final String donePermission = TheyphaniaChatPerms.CMD_DONE.getNodeString();
	
	public DoneCommand()
	{
		plugin.debug("... /done");
		registerCommand();
	}
	
	private void registerCommand()
	{
		Argument argument = new StringArgument("target").overrideSuggestions(plugin::getConversationsFormSenderWithAll);
		new CommandAPICommand("done")
			.withPermission(donePermission)
			.executes((sender, args) ->
			{
				String name = sender.getName();
				String targetName = plugin.getLastPmPlayer(name);
				if (targetName == null)
				{
					plugin.sendCustomMessage(sender, "Ohne offene Konversation kannst du auch nichts schließen.");
					return;
				}
				plugin.removePlayersFromPm(name, targetName);
				plugin.sendCustomMessage(sender, "Deine Konversation mit " + targetName + " wurde erfolgreich geschlossen.");
				CommandSender targetSender = targetName.equalsIgnoreCase("console") ? Bukkit.getConsoleSender() : Bukkit.getPlayer(targetName);
				if (targetSender == null)
				{
					plugin.sendCustomMessage(sender, "Wieder das Cache-Problem.");
					return;
				}
				plugin.sendCustomMessage(targetSender, name + " hat die Konversation mit dir geschlossen.");
			}).register();
		new CommandAPICommand("done")
			.withArguments(argument)
			.withPermission(donePermission)
			.executes((sender, args) ->
			{
				String name = sender.getName();
				String targetName = (String) args[0];
				if (targetName.equalsIgnoreCase("all"))
				{
					plugin.removeAllPmsFromPlayer(name);
					plugin.sendCustomMessage(sender, "Du hast alle deine Konversationen erfolgreich geschlossen.");
					return;
				}
				CommandSender targetSender = targetName.equalsIgnoreCase("console") ? Bukkit.getConsoleSender() : Bukkit.getPlayer(targetName);
				if (targetSender == null || plugin.getConversationsFromSender(sender).length == 0)
				{
					plugin.sendCustomMessage(sender, "Dieser Spieler hat keine Konversation mit dir.");
					return;
				}
				plugin.removePlayersFromPm(name, targetName);
				plugin.sendCustomMessage(sender, "Deine Konversation mit " + targetName + " wurde erfolgreich geschlossen.");
				plugin.sendCustomMessage(targetSender, name + " hat die Konversation mit dir geschlossen.");
			}).register();
	}
}
