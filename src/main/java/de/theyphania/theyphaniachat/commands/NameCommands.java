package de.theyphania.theyphaniachat.commands;

import de.theyphania.theyphaniachat.TheyphaniaChat;
import de.theyphania.theyphaniachat.utils.TheyphaniaChatPerms;
import de.theyphania.theyphaniachat.utils.TheyphaniaPlayer;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.Argument;
import dev.jorel.commandapi.arguments.MultiLiteralArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static net.md_5.bungee.api.ChatColor.RED;

public class NameCommands
{
	private final TheyphaniaChat plugin = TheyphaniaChat.get();
	
	public NameCommands()
	{
		this.registerCommands();
		plugin.debug("... /vorname");
		plugin.debug("... /nachname");
		plugin.debug("... /rufname");
	}
	
	private void registerCommands()
	{
		Argument vorname1Argument = new StringArgument("vorname1").overrideSuggestions(plugin::getVorname1);
		Argument vorname2Argument = new StringArgument("vorname2").overrideSuggestions(plugin::getVorname2);
		Argument vorname3Argument = new StringArgument("vorname3").overrideSuggestions(plugin::getVorname3);
		Argument vorname4Argument = new StringArgument("vorname4").overrideSuggestions(plugin::getVorname4);
		Argument nachnameArgument = new StringArgument("nachname").overrideSuggestions(plugin::getNachname);
		CommandAPICommand vorname1 = new CommandAPICommand("1")
										 .withArguments(vorname1Argument)
										 .withPermission(TheyphaniaChatPerms.CMD_VORNAME.getAppend("1"))
										 .executesPlayer((player, args) ->
										 {
											 setName(player, 1, (String) args[0], "vorname");
										 });
		CommandAPICommand vorname2 = new CommandAPICommand("2")
										 .withArguments(vorname2Argument)
										 .withPermission(TheyphaniaChatPerms.CMD_VORNAME.getAppend("2"))
										 .executesPlayer((player, args) ->
										 {
											 setName(player, 2, (String) args[0], "vorname");
										 });
		CommandAPICommand vorname3 = new CommandAPICommand("3")
										 .withArguments(vorname3Argument)
										 .withPermission(TheyphaniaChatPerms.CMD_VORNAME.getAppend("3"))
										 .executesPlayer((player, args) ->
										 {
											 setName(player, 3, (String) args[0], "vorname");
										 });
		CommandAPICommand vorname4 = new CommandAPICommand("4")
										 .withArguments(vorname4Argument)
										 .withPermission(TheyphaniaChatPerms.CMD_VORNAME.getAppend("4"))
										 .executesPlayer((player, args) ->
										 {
											 setName(player, 4, (String) args[0], "vorname");
										 });
		new CommandAPICommand("vorname")
			.withPermission(TheyphaniaChatPerms.CMD_VORNAME.getNodeString())
			.withSubcommand(vorname1)
			.withSubcommand(vorname2)
			.withSubcommand(vorname3)
			.withSubcommand(vorname4)
			.executes((sender, args) ->
			{
				plugin.sendCustomMessage(sender, "Dieser Befehl erlaubt es dir deine Vornamen festzulegen. Wieviele Vornamen du haben kannst ist von deinem sozialan Stand abhängig. Das Maximum an Vornamen welche man haben kann beträgt 4 und ist ab erreichen des Freiherren-Titels möglich.");
			}).register();
		new CommandAPICommand("nachname")
			.withArguments(nachnameArgument)
			.withPermission(TheyphaniaChatPerms.CMD_NACHNAME.getNodeString())
			.executesPlayer((player, args) ->
			{
				setName(player, 0, (String) args[0], "nachname");
			}).register();
		new CommandAPICommand("nachname")
			.withPermission(TheyphaniaChatPerms.CMD_NACHNAME.getNodeString())
			.executes((sender, args) ->
			{
				plugin.sendCustomMessage(sender, "Dieser Befehl erlaubt es dir deinen Nachnamen bzw. Familiennamen festzulegen.");
			}).register();
		CommandAPICommand rufname1 = new CommandAPICommand("1")
										 .withPermission(TheyphaniaChatPerms.CMD_VORNAME.getAppend("1"))
										 .executesPlayer((player, args) ->
										 {
											 setName(player, 1, "", "rufname");
										 });
		CommandAPICommand rufname2 = new CommandAPICommand("2")
										 .withPermission(TheyphaniaChatPerms.CMD_VORNAME.getAppend("2"))
										 .executesPlayer((player, args) ->
										 {
											 setName(player, 2, "", "rufname");
										 });
		CommandAPICommand rufname3 = new CommandAPICommand("3")
										 .withPermission(TheyphaniaChatPerms.CMD_VORNAME.getAppend("3"))
										 .executesPlayer((player, args) ->
										 {
											 setName(player, 3, "", "rufname");
										 });
		CommandAPICommand rufname4 = new CommandAPICommand("4")
										 .withPermission(TheyphaniaChatPerms.CMD_VORNAME.getAppend("4"))
										 .executesPlayer((player, args) ->
										 {
											 setName(player, 4, "", "rufname");
										 });
		
		new CommandAPICommand("rufname")
			.withPermission(TheyphaniaChatPerms.CMD_RUFNAME.getNodeString())
			.withSubcommand(rufname1)
			.withSubcommand(rufname2)
			.withSubcommand(rufname3)
			.withSubcommand(rufname4)
			.executes((sender, args) ->
			{
				plugin.sendCustomMessage(sender, "Dieser Befehl erlaubt es dir einen deiner Vornamen als Rufnamen festzulegen. '/rufname 2' würde zum Beispiel deinen 2. Vornamen als Rufnamen festlegen. Solltest du noch keinen eigenen Rufnamen festgelegt haben, so ist dein 1. Vorname standardmäßig dein Rufname.");
			}).register();
		List<Argument> adelszusatzArguments = new ArrayList<>();
		adelszusatzArguments.add(new MultiLiteralArgument("von", "van", "fen"));
		new CommandAPICommand("adelszusatz")
			.withPermission(TheyphaniaChatPerms.CMD_ADELSZUSATZ.getNodeString())
			.executes((sender, args) ->
			{
				plugin.sendCustomMessage(sender, "Dieser Befehl erlaubt es dir deinen Adelszusatz festzulegen. Derzeitig möglich sind: von, von der, von dem, van, van der, van dem, fen, fen der, fen dem");
			}).register();
		new CommandAPICommand("adelszusatz")
			.withPermission(TheyphaniaChatPerms.CMD_ADELSZUSATZ.getNodeString())
			.withArguments(adelszusatzArguments)
			.executesPlayer((player, args) ->
			{
				setName(player, 0, (String) args[0], "adelszusatz");
			}).register();
		new CommandAPICommand("adelszusatz")
			.withPermission(TheyphaniaChatPerms.CMD_ADELSZUSATZ.getNodeString())
			.withArguments(adelszusatzArguments)
			.withArguments(new MultiLiteralArgument("der", "dem", "den"))
			.executesPlayer((player, args) ->
			{
				String zusatz = args[0] + " " + args[1];
				setName(player, 0, zusatz, "adelszusatz");
			}).register();
	}
	
	private void setName(Player player, int index, String name, String type)
	{
		TheyphaniaPlayer newPlayer = plugin.getPlayer(player.getUniqueId());
		boolean hasTemRight = newPlayer.hasPermission(TheyphaniaChatPerms.NAME_CHANGE.getNodeString());
		if (type.equalsIgnoreCase("vorname"))
		{
			String vorname = newPlayer.getVornamen()[index - 1];
			if (!vorname.isEmpty() && !vorname.equalsIgnoreCase(name) && !hasTemRight)
			{
				plugin.sendCustomMessage(player, RED + "Bitte eröffne mit '/hilfe' ein Hilfegesuch, um deinen Namen ändern zu lassen.");
				return;
			}
			newPlayer.setPlayerMetaData(type + index, name);
			plugin.sendCustomMessage(player, "Der angegebene Name wurde als " + index + ". Vorname vermerkt.");
		}
		if (type.equalsIgnoreCase("nachname"))
		{
			String nachname = newPlayer.getNachname();
			if (!nachname.isEmpty() && !nachname.equalsIgnoreCase(name) && !hasTemRight)
			{
				plugin.sendCustomMessage(player, RED + "Bitte eröffne mit '/hilfe' ein Hilfegesuch, um deinen Nachnamen ändern zu lassen.");
				return;
			}
			newPlayer.setPlayerMetaData(type, name);
			plugin.sendCustomMessage(player, "Der angegebene Name wurde als Nachname vermerkt.");
		}
		if (type.equalsIgnoreCase("rufname"))
		{
			if (newPlayer.getVornamen()[index - 1].isEmpty())
			{
				plugin.sendCustomMessage(player, RED + " Dein " + index + ". Vorname wurde noch nicht gesetzt, daher kannst du ihn auch nicht als Rufnamen festlegen.");
				return;
			}
			newPlayer.setPlayerMetaData("rufname", String.valueOf(index));
			plugin.sendCustomMessage(player, "Dein " + index + ". Vorname ist nun auch dein Rufname.");
		}
		if (type.equalsIgnoreCase("adelszusatz"))
		{
			if (!newPlayer.getAdelszusatz().isEmpty() && !hasTemRight)
			{
				plugin.sendCustomMessage(player, RED + "Du hast schon einen Adelszusatz gesetzt. Bitte erfrage mit '/hilfe' eine entsprächende Änderung.");
				return;
			}
			newPlayer.setPlayerMetaData(type, '"' + name + '"');
			plugin.sendCustomMessage(player, "Du bist nun ein " + name + " " + newPlayer.getNachname() + ".");
		}
		Bukkit.getScheduler().runTaskLater(plugin, () ->
		{
			plugin.removePlayer(player.getUniqueId());
			plugin.addPlayer(new TheyphaniaPlayer(player));
		}, 10);
	}
}
