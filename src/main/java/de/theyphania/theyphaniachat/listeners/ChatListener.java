package de.theyphania.theyphaniachat.listeners;

import de.theyphania.theyphaniachat.TheyphaniaChat;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static net.md_5.bungee.api.ChatColor.WHITE;

public class ChatListener implements Listener
{
	private final TheyphaniaChat plugin = TheyphaniaChat.get();
	private static ChatListener i;
	
	public static ChatListener get()
	{
		return i;
	}
	
	public ChatListener()
	{
		i = this;
		plugin.debug("... AsyncPlayerChatEvent");
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onAsyncPlayerChat(AsyncPlayerChatEvent e)
	{
		if (e.isCancelled()) return;
		e.setCancelled(true);
		
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTask(plugin, () ->
		{
			player.performCommand(plugin.getChannel(player.getUniqueId()) + " " + e.getMessage());
		});
	}
	
	public void broadcastMessage(TextComponent channel, TextComponent prefix, BaseComponent name, String message, boolean withColor, String permission, double radius, Location location)
	{
		TextComponent componentMessagePre = new TextComponent();
		componentMessagePre.addExtra(channel == null ? new TextComponent("") : channel);
		componentMessagePre.addExtra(prefix == null ? new TextComponent("") : prefix);
		componentMessagePre.addExtra(name == null ? new TextComponent("") : name);
		componentMessagePre.addExtra(WHITE + ": ");
		
		BaseComponent[] componentMessage = plugin.parseOutOfRpChat(message, withColor);
		BaseComponent[] componentMessageFull = new BaseComponent[componentMessage.length + 1];
		componentMessageFull[0] = componentMessagePre;
		System.arraycopy(componentMessage, 0, componentMessageFull, 1, componentMessage.length);
		Bukkit.getConsoleSender().sendMessage(componentMessageFull);
		if (radius < 0 || location == null)
		{
			if (permission == null || permission.isEmpty())
			{
				Bukkit.broadcast(componentMessageFull);
				return;
			}
			for (Player onlinePlayer : Bukkit.getOnlinePlayers())
			{
				if (onlinePlayer.hasPermission(permission)) onlinePlayer.spigot().sendMessage(componentMessageFull);
			}
			return;
		}
		for (Player onlinePlayer : Bukkit.getOnlinePlayers())
		{
			if (location.getWorld() != onlinePlayer.getWorld()) continue;
			if (onlinePlayer.getLocation().distance(location) > radius) continue;
			if (!onlinePlayer.hasPermission(permission)) continue;
			onlinePlayer.spigot().sendMessage(componentMessageFull);
		}
	}
	
	private String parseEmpty(String toParse)
	{
		return toParse.isEmpty() ? "N/A" : toParse;
	}
}
