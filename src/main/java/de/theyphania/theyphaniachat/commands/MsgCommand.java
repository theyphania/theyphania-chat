package de.theyphania.theyphaniachat.commands;

import de.theyphania.theyphaniachat.TheyphaniaChat;
import dev.jorel.commandapi.annotations.Command;
import dev.jorel.commandapi.annotations.Default;
import dev.jorel.commandapi.annotations.Permission;
import dev.jorel.commandapi.annotations.arguments.AGreedyStringArgument;
import dev.jorel.commandapi.annotations.arguments.APlayerArgument;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Command("msg")
@Permission("theyphania.command.msg")
public class MsgCommand
{
	private static final TheyphaniaChat plugin = TheyphaniaChat.get();
	
	@Default
	public static void msg(CommandSender sender)
	{
		plugin.sendCustomMessage(sender, "Dieser Befehl erlaubt es dir eine Nachricht an einen Spieler zu senden.");
	}
	
	@Default
	public static void msg(CommandSender sender, @APlayerArgument Player target, @AGreedyStringArgument String message)
	{
		target.playSound(target.getLocation(), Sound.ENTITY_EVOKER_PREPARE_WOLOLO, 1F, 1F);
		Bukkit.getScheduler().runTaskLater(plugin, () ->
		{
			target.playSound(target.getLocation(), Sound.ENTITY_EVOKER_PREPARE_WOLOLO, 1F, .2F);
		}, 12L);
		plugin.setPlayersInPm(sender.getName(), target.getDisplayName());
		sender.sendMessage(ChatColor.GREEN + "(Zu " + target.getDisplayName() + ") " + ChatColor.GRAY + message);
		target.sendMessage(ChatColor.GREEN + "(Von " + sender.getName() + ") " + ChatColor.GRAY + message);
	}
}
