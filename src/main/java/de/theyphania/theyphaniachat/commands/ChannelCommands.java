package de.theyphania.theyphaniachat.commands;

import de.theyphania.theyphaniachat.TheyphaniaChat;
import de.theyphania.theyphaniachat.listeners.ChatListener;
import de.theyphania.theyphaniachat.utils.TheyphaniaChatPerms;
import de.theyphania.theyphaniachat.utils.TheyphaniaPlayer;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.configuration.ConfigurationSection;

import java.util.List;

public class ChannelCommands
{
	private final TheyphaniaChat plugin = TheyphaniaChat.get();
	private final ChatListener chat = ChatListener.get();
	
	public ChannelCommands()
	{
		registerCommands();
		plugin.debug("... ChannelCommands");
	}
	
	private void registerCommands()
	{
		ConfigurationSection channels = plugin.getConfig().getConfigurationSection("channels");
		if (channels == null) return;
		for (String s : channels.getKeys(false))
		{
			ConfigurationSection channel = channels.getConfigurationSection(s);
			if (channel == null) continue;
			String name = channel.getString("name");
			String formatPrefixString = channel.getString("format.prefix");
			String formatMessageString = channel.getString("format.message");
			ChatColor formatPrefix = ChatColor.of(formatPrefixString == null ? "white" : formatPrefixString);
			ChatColor formatMessage = ChatColor.of(formatMessageString == null ? "white" : formatMessageString);
			new CommandAPICommand(s)
				.withArguments(new GreedyStringArgument("Nachricht"))
				.withPermission(TheyphaniaChatPerms.CHANNELS.getAppend(s))
				.executesPlayer((player, args) ->
				{
					StringBuilder sb = new StringBuilder();
					for (Object string : args)
					{
						sb.append(string).append(" ");
					}
					String message = sb.toString();
					TheyphaniaPlayer tPlayer = new TheyphaniaPlayer(player);
					TextComponent componentChannelLeft = new TextComponent("[");
					componentChannelLeft.setColor(ChatColor.DARK_GRAY);
					TextComponent componentChannelMiddle = new TextComponent(name);
					componentChannelMiddle.setColor(formatPrefix);
					TextComponent componentChannelRight = new TextComponent("]");
					componentChannelRight.setColor(ChatColor.DARK_GRAY);
					TextComponent componentChannel = new TextComponent();
					componentChannel.addExtra(componentChannelLeft);
					componentChannel.addExtra(componentChannelMiddle);
					componentChannel.addExtra(componentChannelRight);
					componentChannel.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new Text(ChatColor.GRAY + "Typ: " + ChatColor.WHITE + (channel.getBoolean("isGlobal") ? "Global" : "Lokal"))));
					double radius = -1;
					ConfigurationSection radiusSection = plugin.getConfig().getConfigurationSection("radiusKeyMap");
					if (radiusSection != null && !channel.getBoolean("isGlobal"))
					{
						double defaultRadius = 0;
						for (String radiusString : radiusSection.getKeys(false))
						{
							List<String> keyWords = radiusSection.getStringList(radiusString);
							for (String keyWord : keyWords)
							{
								if (keyWord.equalsIgnoreCase("default"))
									defaultRadius = Double.parseDouble(radiusString);
								if (!message.startsWith("*" + keyWord)) continue;
								radius = Double.parseDouble(radiusString);
								break;
							}
							if (radius >= 0) break;
						}
						if (radius < 0) radius = defaultRadius;
					}
					TextComponent componentPrefix = new TextComponent(tPlayer.getPrefix());
					chat.broadcastMessage(name == null ? null : componentChannel, componentPrefix, tPlayer.getNameWithHoverText(), formatMessage + message, player.hasPermission(TheyphaniaChatPerms.CHAT_COLOR.getNodeString()), TheyphaniaChatPerms.CHANNELS.getAppend(s), radius, player.getLocation());
				}).register();
			if (channel.getBoolean("isGlobal")) continue;
			new CommandAPICommand(s)
				.withPermission(TheyphaniaChatPerms.CHANNELS.getAppend(s))
				.executesPlayer((player, args) ->
				{
					plugin.setChannel(player.getUniqueId(), s);
					plugin.sendCustomMessage(player, "Du schreibst nun im " + name + "-Channel.");
				}).register();
		}
	}
}
