package de.theyphania.theyphaniachat.listeners;

import de.theyphania.theyphaniachat.TheyphaniaChat;
import de.theyphania.theyphaniachat.utils.ScoreboardManager;
import de.theyphania.theyphaniachat.utils.TheyphaniaPlayer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.ComponentBuilder.FormatRetention;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import static net.md_5.bungee.api.ChatColor.DARK_GRAY;
import static net.md_5.bungee.api.ChatColor.GOLD;

public class JoinListener implements Listener
{
	private final TheyphaniaChat plugin = TheyphaniaChat.get();
	private final ScoreboardManager scoreboardManager = ScoreboardManager.get();
	
	private static JoinListener i;
	
	public JoinListener()
	{
		i = this;
		plugin.debug("... PlayerJoinEvent");
		plugin.debug("... PlayerQuitEvent");
		Bukkit.getPluginManager().registerEvents(this, TheyphaniaChat.get());
	}
	
	public static JoinListener get()
	{
		return i;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		Player player = e.getPlayer();
		TheyphaniaPlayer newPlayer = new TheyphaniaPlayer(player);
		plugin.addPlayer(newPlayer);
		scoreboardManager.registerNewTeam(player);
		String rufname = newPlayer.getRufname().isEmpty() ? "" : newPlayer.getRufname() + " ";
		String adelszusatz = newPlayer.getAdelszusatz().isEmpty() ? "" : newPlayer.getAdelszusatz() + " ";
		String nachname = newPlayer.getNachname().isEmpty() ? "" : newPlayer.getNachname() + " ";
		BaseComponent[] displayName = rufname.isEmpty() ? new ComponentBuilder(newPlayer.getNameWithHoverText()).append(" ").create() : new ComponentBuilder("(").append(newPlayer.getNameWithHoverText()).append(") ").create();
		e.setJoinMessage("");
		Bukkit.broadcast(new ComponentBuilder(newPlayer.getTitle()).color(newPlayer.getStandColor()).append(" ").append(rufname).append(adelszusatz).append(nachname).append(displayName).retain(FormatRetention.FORMATTING).append(DARK_GRAY + "hat " + GOLD + "Velden" + DARK_GRAY + " betreten.").create());
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e)
	{
		Player player = e.getPlayer();
		scoreboardManager.unregisterTeam(player);
		TheyphaniaPlayer newPlayer = plugin.getPlayer(player.getUniqueId());
		String rufname = newPlayer.getRufname().isEmpty() ? "" : newPlayer.getRufname() + " ";
		String adelszusatz = newPlayer.getAdelszusatz().isEmpty() ? "" : newPlayer.getAdelszusatz() + " ";
		String nachname = newPlayer.getNachname().isEmpty() ? "" : newPlayer.getNachname() + " ";
		BaseComponent[] displayName = rufname.isEmpty() ? new ComponentBuilder(newPlayer.getNameWithHoverText()).append(" ").create() : new ComponentBuilder("(").append(newPlayer.getNameWithHoverText()).append(") ").create();
		e.setQuitMessage("");
		Bukkit.broadcast(new ComponentBuilder(newPlayer.getTitle()).color(newPlayer.getStandColor()).append(" ").append(rufname).append(adelszusatz).append(nachname).append(displayName).retain(FormatRetention.FORMATTING).append(DARK_GRAY + "hat " + GOLD + "Velden" + DARK_GRAY + " verlassen.").create());
		plugin.removePlayer(player.getUniqueId());
		plugin.removeAllPmsFromPlayer(player.getDisplayName());
	}
}
