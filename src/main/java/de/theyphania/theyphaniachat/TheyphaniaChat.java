package de.theyphania.theyphaniachat;

import de.theyphania.theyphaniachat.commands.ChannelCommands;
import de.theyphania.theyphaniachat.commands.DoneCommand;
import de.theyphania.theyphaniachat.commands.MsgCommand;
import de.theyphania.theyphaniachat.commands.NameCommands;
import de.theyphania.theyphaniachat.commands.RCommand;
import de.theyphania.theyphaniachat.commands.ReCommand;
import de.theyphania.theyphaniachat.listeners.ChatListener;
import de.theyphania.theyphaniachat.listeners.JoinListener;
import de.theyphania.theyphaniachat.utils.ScoreboardManager;
import de.theyphania.theyphaniachat.utils.TheyphaniaPlayer;
import dev.jorel.commandapi.CommandAPI;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang.StringUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TheyphaniaChat extends TheyphaniaChatPlugin
{
	private final LinkedHashMap<UUID, TheyphaniaPlayer> theyphaniaPlayerMap = new LinkedHashMap<>();
	private final LinkedHashMap<UUID, String> channelMap = new LinkedHashMap<>();
	private LinkedHashMap<String, List<String>> privateMessages = new LinkedHashMap<>();
	private YamlConfiguration config;
	private File f;
	private ScoreboardManager scoreboardManager;
	
	private static TheyphaniaChat i;
	
	public TheyphaniaChat()
	{
		i = this;
	}
	
	public static TheyphaniaChat get()
	{
		return i;
	}
	
	@Override
	public void onEnableInner()
	{
		log("Loading Config...");
		loadConfig();
		log("Config loaded successfully.");
		debug("Registering custom scoreboard...");
		scoreboardManager = new ScoreboardManager();
		scoreboardManager.enableScoreboardScheduler();
		debug("Custom scoreboard registered successfully.");
		debug("Registering Listeners for...");
		new JoinListener();
		new ChatListener();
		debug("All Listeners registered successfully.");
		debug("Registering Commands...");
		new NameCommands();
		new ChannelCommands();
		debug("... /msg");
		CommandAPI.unregister("msg", true);
		CommandAPI.registerCommand(MsgCommand.class);
		debug("... /r");
		CommandAPI.registerCommand(RCommand.class);
		new ReCommand();
		new DoneCommand();
		debug("All Commands registered successfully.");
	}
	
	@Override
	public void onDisable()
	{
		scoreboardManager.shutdown();
	}
	
	public void setPlayersInPm(String player1, String player2)
	{
		List<String> tempList = privateMessages.containsKey(player1) ? privateMessages.get(player1) : new ArrayList<>();
		tempList.remove(player2);
		tempList.add(player2);
		privateMessages.put(player1, tempList);
		tempList = privateMessages.containsKey(player2) ? privateMessages.get(player2) : new ArrayList<>();
		tempList.remove(player1);
		tempList.add(player1);
		privateMessages.put(player2, tempList);
	}
	
	public void removePlayersFromPm(String player1, String player2)
	{
		List<String> tempList = privateMessages.get(player1);
		tempList.remove(player2);
		tempList = privateMessages.get(player2);
		tempList.remove(player1);
	}
	
	public void removeAllPmsFromPlayer(String player1)
	{
		if (!privateMessages.containsKey(player1)) return;
		List<String> tempList = privateMessages.get(player1);
		privateMessages.remove(player1);
		if (tempList.isEmpty()) return;
		for (String s : tempList)
		{
			if (!privateMessages.containsKey(s)) continue;
			List<String> tempList2 = privateMessages.get(s);
			tempList2.remove(player1);
			privateMessages.put(s, tempList2);
		}
	}
	
	public String getLastPmPlayer(String player1)
	{
		if (!privateMessages.containsKey(player1)) return null;
		List<String> tempList = privateMessages.get(player1);
		if (tempList.isEmpty()) return null;
		return tempList.get(tempList.size() - 1);
	}
	
	public String[] getConversationsFromSender(CommandSender sender)
	{
		String name = sender.getName();
		if (!privateMessages.containsKey(name)) return new String[0];
		String[] arr = new String[privateMessages.get(name).size()];
		return privateMessages.get(name).toArray(arr);
	}
	
	public String[] getConversationsFormSenderWithAll(CommandSender sender)
	{
		String[] arr = getConversationsFromSender(sender);
		if (arr.length < 1) return new String[0];
		String[] arr2 = new String[arr.length + 1];
		arr2[0] = "all";
		System.arraycopy(arr, 0, arr2, 1, arr.length);
		return arr2;
	}
	
	public void setChannel(UUID uuid, String channel)
	{
		channelMap.put(uuid, channel);
	}
	
	public void removeChannel(UUID uuid)
	{
		channelMap.remove(uuid);
	}
	
	public String getChannel(UUID uuid)
	{
		return channelMap.getOrDefault(uuid, config.getString("default-channel"));
	}

	public void reloadConfig()
	{
		loadConfig();
	}

	@Override
	public YamlConfiguration getConfig()
	{
		return config;
	}
	
	private void loadConfig()
	{
		if (getDataFolder().mkdirs()) log("No plugin folder found. Creating new one...");
		f = new File(getDataFolder(), "config.yml");
		if (!f.exists())
		{
			log("No custom config file found. Copying defaults...");
			InputStream resourcesConfig = getResource("config.yml");
			try
			{
				if (resourcesConfig == null)
				{
					log(Level.WARNING, "No default configuration file found. Perhaps your JAR-file is corrupted?");
					suicide();
					return;
				}
				Files.copy(resourcesConfig, f.toPath());
				log("Defaults copied successfully.");
			}
			catch (IOException e)
			{
				error(e);
			}
		}
		config = YamlConfiguration.loadConfiguration(f);
		if (config.getConfigurationSection("channels") == null)
		{
			config.set("channels.rp.isGlobal", false);
			config.set("channels.rp.name", "RP");
			config.set("channels.rp.format.prefix", "gray");
			config.set("channels.rp.format.message", "white");
		}
		if (!config.contains("debug"))
		{
			config.set("debug", false);
		}
		if (!config.contains("default-channel"))
		{
			config.set("default-channel", "rp");
		}
		sc();
		config = YamlConfiguration.loadConfiguration(f);
	}

	private void sc()
	{
		try
		{
			config.save(this.f);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void sendCustomMessage(CommandSender sender, String message)
	{
		TextComponent prefix1 = new TextComponent("// ================== [");
		TextComponent prefix2_1 = new TextComponent("system");
		TextComponent prefix2_2 = new TextComponent("::");
		TextComponent prefix2_3 = new TextComponent("chat");
		TextComponent prefix3 = new TextComponent("] ================== //");
		
		prefix1.setColor(ChatColor.DARK_GRAY);
		prefix2_1.setColor(ChatColor.GOLD);
		prefix2_2.setColor(ChatColor.WHITE);
		prefix2_3.setColor(ChatColor.GOLD);
		prefix3.setColor(ChatColor.DARK_GRAY);
		
		TextComponent prefix = new TextComponent();
		prefix.addExtra(prefix1);
		prefix.addExtra(prefix2_1);
		prefix.addExtra(prefix2_2);
		prefix.addExtra(prefix2_3);
		prefix.addExtra(prefix3);
		
		TextComponent msg = new TextComponent(StringUtils.center(" " + message, 30));
		msg.setColor(ChatColor.GRAY);
		
		sender.spigot().sendMessage(prefix);
		sender.spigot().sendMessage(msg);
		sender.spigot().sendMessage(prefix);
	}
	
	public void addPlayer(TheyphaniaPlayer player)
	{
		theyphaniaPlayerMap.put(player.getPlayer().getUniqueId(), player);
	}
	
	public void removePlayer(UUID uuid)
	{
		theyphaniaPlayerMap.remove(uuid);
	}
	
	public TheyphaniaPlayer getPlayer(UUID uuid)
	{
		return theyphaniaPlayerMap.get(uuid);
	}
	
	private String[] getNamesForPlayerFromSender(CommandSender sender)
	{
		if (!(sender instanceof Player)) return new String[0];
		Player player = (Player) sender;
		return getPlayer(player.getUniqueId()).getNames();
	}
	
	public String[] getVorname1(CommandSender sender)
	{
		return new String[]{getNamesForPlayerFromSender(sender)[0]};
	}
	
	public String[] getVorname2(CommandSender sender)
	{
		return new String[]{getNamesForPlayerFromSender(sender)[1]};
	}
	
	public String[] getVorname3(CommandSender sender)
	{
		return new String[]{getNamesForPlayerFromSender(sender)[2]};
	}
	
	public String[] getVorname4(CommandSender sender)
	{
		return new String[]{getNamesForPlayerFromSender(sender)[3]};
	}
	
	public String[] getNachname(CommandSender sender)
	{
		return new String[]{getNamesForPlayerFromSender(sender)[5]};
	}
	
	public BaseComponent[] parseChatColor(String message)
	{
		TextComponent messageComponent = new TextComponent();
		StringBuffer messageBuffer = new StringBuffer();
		Pattern customHexColorPattern = Pattern.compile("&#[0-9a-fA-F]{6}");
		Matcher customHexColorMatcher = customHexColorPattern.matcher(ChatColor.translateAlternateColorCodes('&', message));
		List<ChatColor> customHexColorList = new ArrayList<>();
		while (customHexColorMatcher.find())
		{
			String repString = customHexColorMatcher.group();
			if (repString != null)
			{
				customHexColorMatcher.appendReplacement(messageBuffer, "§§0§§");
				customHexColorList.add(ChatColor.of(repString.substring(1)));
			}
		}
		customHexColorMatcher.appendTail(messageBuffer);
		String tempString = messageBuffer.toString();
		String[] tempArr = tempString.split("§§0§§");
		messageComponent.setText(tempArr[0]);
		for (int i = 1; i < tempArr.length; i++)
		{
			TextComponent tempComponent = new TextComponent(tempArr[i]);
			tempComponent.setColor(customHexColorList.get(i - 1));
			messageComponent.addExtra(tempComponent);
		}
		return new ComponentBuilder(messageComponent).create();
	}
	
	public BaseComponent[] parseOutOfRpChat(String toParse, boolean withColor)
	{
		int countBrackets = StringUtils.countMatches(toParse, "((") - StringUtils.countMatches(toParse, "))");
		if (countBrackets > 0)
		{
			StringBuilder toParseBuilder = new StringBuilder(toParse);
			for (int i = 0; i < countBrackets; i++) toParseBuilder.append("§7§o))");
			toParse = toParseBuilder.toString();
		}
		toParse = toParse.replaceAll("\\(\\(", "§7§o((").replaceAll("\\)\\)", "))§f");
		return withColor ? parseChatColor(toParse) : new ComponentBuilder(toParse).create();
	}
}
