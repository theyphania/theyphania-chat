package de.theyphania.theyphaniachat.utils;

import de.theyphania.theyphaniachat.TheyphaniaChat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScoreboardManager
{
	private final TheyphaniaChat plugin = TheyphaniaChat.get();
	private final Scoreboard mainBoard;
	private int taskId = -1;
	private BukkitScheduler bukkitScheduler = Bukkit.getScheduler();
	private final long SCHEDULER_PERIOD = 1_200L;
	
	private static ScoreboardManager i;
	
	public ScoreboardManager()
	{
		i = this;
		mainBoard = Bukkit.getScoreboardManager().getMainScoreboard();
	}
	
	public static ScoreboardManager get()
	{
		return i;
	}
	
	public void enableScoreboardScheduler()
	{
		taskId = bukkitScheduler.scheduleSyncRepeatingTask(plugin, () ->
		{
			for (Player onlinePlayer : Bukkit.getOnlinePlayers())
			{
				registerNewTeam(onlinePlayer);
			}
		}, 0L, SCHEDULER_PERIOD);
	}
	
	public void registerNewTeam(Player player)
	{
		TheyphaniaPlayer theyphaniaPlayer = plugin.getPlayer(player.getUniqueId());
		String playerName = player.getName();
		String teamName = theyphaniaPlayer.getSortOrder();
		Team team = mainBoard.getTeam(teamName);
		if (team == null)
		{
			team = mainBoard.registerNewTeam(teamName);
		}
		team.setColor(theyphaniaPlayer.getBukkitStandColor());
		team.addEntry(playerName);
	}
	
	public void unregisterTeam(Player player)
	{
		TheyphaniaPlayer theyphaniaPlayer = plugin.getPlayer(player.getUniqueId());
		String teamName = theyphaniaPlayer.getSortOrder();
		Team team = mainBoard.getTeam(teamName);
		if (team == null || team.getSize() > 1) return;
		team.unregister();
	}
	
	public void shutdown()
	{
		if (taskId != -1) bukkitScheduler.cancelTask(taskId);
		for (Team team : mainBoard.getTeams())
		{
			team.unregister();
		}
	}
}
