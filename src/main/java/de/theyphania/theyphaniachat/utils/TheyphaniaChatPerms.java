package de.theyphania.theyphaniachat.utils;

public enum TheyphaniaChatPerms
{
	CMD_VORNAME("command.vorname"),
	CMD_NACHNAME("command.nachname"),
	CMD_RUFNAME("command.rufname"),
	CMD_ADELSZUSATZ("command.adelszusatz"),
	CMD_RE("command.re"),
	CMD_DONE("command.done"),
	CHANNELS("channels"),
	NAME_CHANGE("name.change"),
	CHAT_COLOR("chat.color");
	
	private final String node;
	
	TheyphaniaChatPerms(String node)
	{
		this.node = node;
	}
	
	@org.jetbrains.annotations.NotNull
	@org.jetbrains.annotations.Contract(pure = true)
	public String getNodeString()
	{
		return "theyphania." + node;
	}
	
	public String getAppend(String append)
	{
		return getNodeString() + "." + append;
	}
}
