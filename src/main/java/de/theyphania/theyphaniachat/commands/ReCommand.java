package de.theyphania.theyphaniachat.commands;

import de.theyphania.theyphaniachat.TheyphaniaChat;
import de.theyphania.theyphaniachat.utils.TheyphaniaChatPerms;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.Argument;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

public class ReCommand
{
	private final TheyphaniaChat plugin = TheyphaniaChat.get();
	private final String rePermission = TheyphaniaChatPerms.CMD_RE.getNodeString();
	
	public ReCommand()
	{
		plugin.debug("... /re");
		registerCommand();
	}
	
	private void registerCommand()
	{
		Argument argument = new StringArgument("target").overrideSuggestions(plugin::getConversationsFromSender);
		new CommandAPICommand("re")
			.withArguments(argument)
			.withArguments(new GreedyStringArgument("message"))
			.withPermission(rePermission)
			.executesPlayer((player, args) ->
			{
				String name = player.getDisplayName();
				String target = (String) args[0];
				CommandSender targetSender = target.equalsIgnoreCase("console") ? Bukkit.getConsoleSender() : Bukkit.getPlayer(target);
				if (targetSender == null || plugin.getConversationsFromSender(player).length == 0)
				{
					plugin.sendCustomMessage(player, "Du hast keine Konversationen die du wiederaufnehmen könntest.");
					return;
				}
				plugin.setPlayersInPm(name, target);
				player.sendMessage(ChatColor.GREEN + "(Zu " + target + ") " + ChatColor.GRAY + args[1]);
				targetSender.sendMessage(ChatColor.GREEN + "(Von " + name + ") " + ChatColor.GRAY + args[1]);
			}).register();
		new CommandAPICommand("re")
			.withPermission(rePermission)
			.executes((sender, args) ->
			{
				plugin.sendCustomMessage(sender, "Dieser Befehl ermöglicht es dir deine aktive Konversation zu wechseln.\nHandhabung:\n/re <Spieler> <Nachricht>");
			}).register();
	}
}
